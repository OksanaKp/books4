<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>HTML5 и CSS3 сайт</title>
  <link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet"  type="text/css"  media="all">
  <script src="${pageContext.request.contextPath}/js/extLibs/http_code.jquery.com_jquery-1.10.2.js"></script>
  <script src="${pageContext.request.contextPath}/js/extLibs/http_code.jquery.com_ui_1.10.4_jquery-ui.js"></script>
  <script src="${pageContext.request.contextPath}/js/extLibs/http_ajax.googleapis.com_ajax_libs_jquery_1.12.4_jquery.js"></script>
</head>
<body>
<%@ include file="templates/headerWithMenu.jsp" %>

  <div id="top">
    <h1>Время читать!</h1>
    <h3>Лучшие книги у нас!</h3>
  </div>
  <div id="katalogs">
	   <h1 style="text-align:center">Поиск книг</h1>
       <form id="form_input" method="post" action="${pageContext.request.contextPath}/catalog">
           <label for="genre">Введите название жанра </label><br>
           <input type="text" placeholder="Жанр" name="genre" id="genre"
                  pattern="[А-Яа-яЁё]{2,25}">
           <button  id="mess_send" class="btn" type="submit" class="btnForm" style="float:none; display: inline">Найти</button>
       </form>

       <table class="tabl">
           <thead>
           <tr>
               <th>#</th>
               <th>Название</th>
               <th>Автор</th>
               <th>Жанр</th>
               <th>Кол. страниц</th>
           </tr>
           </thead>
           <tbody>
           <%int index = 1;%>
           <c:forEach var="book" items="${BookList}">
               <tr>
                   <td><%=index++%></td>
                   <td>${book.getTitle()}</td>
                   <td>${book.getAuthor()}</td>
                   <td>${book.genre}</td>
                   <td>${book.pageCount}</td>
               </tr>
           </c:forEach>
           </tbody>
       </table>
  </div>
  <%@ include file="templates/footer.jsp" %>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
//Изменение свойств шапки сайта
// <!--Функция, выполняющаяся при скролле-->
    $(document).on("scroll", function () {
      if($(window).scrollTop() === 0)
        $("header").removeClass("fixed");
      else/*если расстояние, на которое проскроллили!=0*/
        $("header").attr("class", "fixed");/*добавляем класс*/
    });
  </script>
</body>
</html>