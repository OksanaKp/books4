type="text/javascript"; charset="utf-8";
var req;
function checkEnteredMail() {
     if ($("#mail").val().length < 9){
         return false;
     }
    {
        $.ajax({
            // Название файла, в котором будем проверять email на существование в базе данных
            url: "checkEnteredMail",
            // Указывываем каким методом будут переданы данные
            type: "POST",
            // Указывываем в формате JSON какие данные нужно передать
            data: {
                checkedMail: $("#mail").val()
            },
            // Тип содержимого которого мы ожидаем получить от сервера.
            dataType: "html",
            // Функция которая будет выполнятся перед отправкой данных
            beforeSend: function () {
                $('#checkmailmessage').text('Проверяется...');
            },
            // Функция которая будет выполнятся после того как все данные будут успешно получены.
            success: function (data) {
                //Полученный ответ помещаем внутри тега spancheckMailMessage
                $('#checkmailmessage').html(data);
            }
        });
    }
}


