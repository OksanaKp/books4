<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page pageEncoding="UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
<jsp:useBean id="userBean" class="models.Users" scope="session"/>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>HTML5 и CSS3 сайт</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
  <script src="${pageContext.request.contextPath}/js/extLibs/http_code.jquery.com_jquery-1.10.2.js"></script>
  <script src="${pageContext.request.contextPath}/js/extLibs/http_code.jquery.com_ui_1.10.4_jquery-ui.js"></script>
  <script src="${pageContext.request.contextPath}/js/extLibs/http_ajax.googleapis.com_ajax_libs_jquery_1.12.4_jquery.js"></script>
</head>
<body>
  <%@ include file="../templates/headerWithMenu.jsp" %>
  <div id="top">
    <h1>Время читать!</h1>
    <h3>Лучшие книги у нас!</h3>
  </div>
  <div class="accountData">
    <div style="padding-left: 20%">
       <h1> Мои данные</h1>
       <div style="padding-bottom: 0; padding-top: 30px">
          <table>
             <tr>
               <td>Имя:</td>
               <td><jsp:getProperty name="userBean" property="firstName"/></td>
             </tr>
             <tr class="altr">
               <td>Фамилия:</td>
               <td><jsp:getProperty name="userBean" property="lastName"/></td>
             </tr>
             <tr>
               <td>Е-mail:</td>
               <td>${sessionScope.userBean.email}</td>
               <%--                          <td><jsp:getProperty name="userBean" property="email"/></td>--%>
             </tr>
          </table>
          <button type="submit" onClick='location.href="/books/user/edit.jsp"' id="mess_send" class="btn"  style="width: 180px; margin: 20px auto;">Редактировать</button>
       </div>
    </div>
   </div>
  <%@ include file="../templates/footer.jsp" %>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
<!--Функция, выполняющаяся при скролле-->
    $(document).on("scroll", function () {
      if($(window).scrollTop() === 0)
        $("header").removeClass("fixed");
      else/*если расстояние, на которое проскроллили!=0*/
        $("header").attr("class", "fixed");/*добавляем класс*/
    });
  </script>
</body>
</html>