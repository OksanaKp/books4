<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page pageEncoding="UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
<jsp:useBean id="userBean" class="models.Users" scope="session"/>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>HTML5 и CSS3 сайт</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
  <script src="${pageContext.request.contextPath}/js/extLibs/http_code.jquery.com_jquery-1.10.2.js"></script>
  <script src="${pageContext.request.contextPath}/js/extLibs/http_code.jquery.com_ui_1.10.4_jquery-ui.js"></script>
  <script src="${pageContext.request.contextPath}/js/extLibs/http_ajax.googleapis.com_ajax_libs_jquery_1.12.4_jquery.js"></script>
</head>
<body>
<%@ include file="../templates/headerWithMenu.jsp" %>
  <div id="top">
	<h1>Время читать!</h1>
	<h3>Лучшие книги у нас!</h3>
  </div>
  <div id="contacts" class="contactPage">
	<form id="form_input" method="post" action="${pageContext.request.contextPath}/editing">
		<h1> Мои данные</h1>
		<br>
		<label for="firstname"  title = "Только русские буквы">Ваше имя<span>*</span></label><br>
		<div>
			<input type="text" name="firstname" id="firstname" class="field" pattern="[А-Яа-яЁё]{2,30}"
				   value = ${sessionScope.userBean.firstName}><br>
		</div>
		<label for="lastname" title = "Только русские буквы">Ваша фамилия<span>*</span> </label><br>
		<div>
			<input type="text" name="lastname" id="lastname" class="field" pattern="[А-Яа-яЁё]{2,30}"
				   value = ${sessionScope.userBean.lastName}><br>
		</div>
		<label for="mail">Логин <span>*</span></label><br>
		<div>
			<input type="text" name="mail" id="mail" class="field"
				   pattern="[\S+@\S+\.\S+]{9,40}" value = ${sessionScope.userBean.email}><br>
		</div>
		<label for="password1">Пароль <span>*</span></label><br>
		<div>
			<input type="password" name="password1" id="password1" class="field" autocomplete="new-password"
				   pattern="[\w]{3,30}" value = ${sessionScope.userBean.password}><br>
		</div>
		<label for="password2">Повторите пароль<span>*</span></label><br>
		<div>
			<input type="password" name="password2" id="password2" class="field" autocomplete="new-password"
				   pattern="[\w]{3,30}" value = ${sessionScope.userBean.password}><br>
		</div> <br>
		<button type="submit" id="mess_send" class="btn" style="width: 180px; margin: 20px auto;">Отправить</button>
		<br>
		<br>
	</form>
  </div>
  <%@ include file="../templates/footer.jsp" %>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
	<!--Функция, выполняющаяся при скролле-->
	$(document).on("scroll", function () {
		if($(window).scrollTop() === 0)
			$("header").removeClass("fixed");
		else/*если расстояние, на которое проскроллили!=0*/
			$("header").attr("class", "fixed");/*добавляем класс*/
	});
  </script>
</body>
</html>