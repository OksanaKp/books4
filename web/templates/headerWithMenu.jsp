<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<header>
    <div id="logo" onclick='location.href="${pageContext.request.contextPath}/main"'>
        <span>Books</span>
    </div>
    <div id="about">
        <a href="${pageContext.request.contextPath}/main" title="Возможности" onclick="slowScroll('#mainController')">Возможности</a>
        <a href="${pageContext.request.contextPath}/catalog" title="Каталог">Каталог</a>
        <a href="#" title="Контакты" onclick="slowScroll('#contacts')">Контакты</a>
        <a href="${pageContext.request.contextPath}/user/myaccount.jsp" title="Войти">Мой лк</a>
        <a href="${pageContext.request.contextPath}/logout" title="Выйти"> ${sessionScope.doLogout}</a>
    </div>
</header>