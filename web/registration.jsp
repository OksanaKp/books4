<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>HTML5 и CSS3 сайт</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
	<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
	<script src="${pageContext.request.contextPath}/js/checkForm.js"></script>
	<script src="${pageContext.request.contextPath}/js/checkForm.js"></script>
	<script src="${pageContext.request.contextPath}/ajax/checkMail.js"></script>
	<script src="${pageContext.request.contextPath}/jquer/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/ajax/checkMail.js"></script>
</head>
<body> 
 <div id="contacts" class="contactPage">
    <div style="text-align: center;"><h5>Регистрация</h5></div>
	 <form action="userReg"  method="post" id="form_input"  autocomplete="off">
		<label for="firstname"  title = "Только русские буквы">Ваше имя<span>*</span></label><br>
		<div>
			<input type="text" name="firstname" id="firstname" class="field" placeholder="Имя"  pattern="[А-Яа-яЁё]{2,30}"><br>
		</div>
		<label for="lastname" title = "Только русские буквы">Ваша фамилия<span>*</span> </label><br>
		<div>
			<input type="text" name="lastname" id="lastname" class="field" placeholder="Фамилия"  pattern="[А-Яа-яЁё]{2,30}"><br>
		</div>
		<label for="mail">Логин <span>*</span></label><br>
		<div>
			<input type="text" placeholder="Введите логин" name="mail" id="mail" class="field"
			pattern="[\S+@\S+\.\S+]{9,40}" onkeyup="checkEnteredMail();"><br>
		</div>
		<span id="checkmailmessage"></span>
		<br>
		<label for="password1">Пароль <span>*</span></label><br>
		<div>
			<input type="password" placeholder="Введите пароль" name="password1" id="password1" class="field" autocomplete="new-password"
				pattern="[\w]{3,30}"><br>
		</div>
		<label for="password2">Повторите пароль<span>*</span></label><br>
		<div>
			<input type="password" name="password2" id="password2" class="field" placeholder="Введите пароль" autocomplete="new-password"
				   pattern="[\w]{3,30}"><br>
		</div> <br>
		<label for="captha">Введите символы:</label><br>
		<div>
			<input type="text" name="captha" id="captha"  class="field" placeholder="Введите символы" ><br>
		</div>
		<h2 id="capthaCode">код</h2><br>
		<script>
			randomString(5);
		</script>
		<a  onClick=randomString(5) >Другой код<br><br></a>
		<button  id="mess_send" class="btn" type="submit" class="btnForm">Отправить</button>
	  <br><br><br>
	<a href="main" title="На главную" class=" heading">На главную</a>
    </form>
  </div>
 <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
 <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
 <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
</body>
