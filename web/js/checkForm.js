
var firstname
var lastname
var mail
var password1
var password2
var fields
var captha
var checP;
var isFilled = true;
/*создать элемент, содержащий текст ошибки text*/
var generateErrorMessage = function (text) {
    var error = document.createElement('div')
    error.className = 'error'
    error.style.color = 'red'
    error.innerHTML = text
    return error
}
/*сгенерировать уведомление об ошибке для пользователя*/
function genError(field, message){
    field.style.border = "2px solid red";
    var error = generateErrorMessage(message);
    field.parentElement.insertBefore(error, field);
    isFilled = false;
}
/*удалить все имеющиеся сообщения об ошибках*/
var removeValidation = function () {
    var errors = document.querySelectorAll('.error')
    for (var i = 0; i < errors.length; i++) {
        errors[i].remove()
    }
    isFilled = true;
}

/*проверка на наличие незаполненных полей формы*/
var isFieldsFull = function () {
    var errCount = 0
    for (var i = 0; i < fields.length; i++) {
        if (fields[i].value.length < 1) {
            genError(fields[i], "Заполните поле!")
            errCount++;
        }
        else{fields[i].style.border = "1px solid black";}
    }
    if (errCount!=0) return false;
    else
        return true;
}


var checkPassword = function () {
    if (password1.value.length < 3) {
        genError(password1, "Пароль слишком короткий!")
    }
    else
    if (password1.value.length > 30) {
        genError(password1, "Пароль слишком короткий!")
    }
    else{
        password1.style.border = "1px solid black";
    }
}

function matchPasswords(){
    if (password1.value !== password2.value) {
        genError(password1, "Пароли не совпадают!");
        genError(password2, "Пароли не совпадают!");
    }
    else{
        password1.style.border = "1px solid black";
        password2.style.border = "1px solid black";
    }
}

function checkCyrillic(str){
    return /^[а-яёА-ЯЁ]*$/i.test(str);/*находится ли шаблон в строке*/
}

var checkName = function () {
    if (firstname.value.length < 2) {
        genError(firstname, "Введите имя!")
    }
    else
    if (!checkCyrillic(firstname.value)) {
        genError(firstname, "Только русские символы!")

    }
    else
    if (firstname.value.length > 30) {
        genError(firstname, "Некорректно указано имя!")
    }
    else
    {
        firstname.style.border = "1px solid black";
    }
}

var checkLastname = function () {
    if (lastname.value.length < 2) {
        genError(lastname, "Введите фамилию!")
    }
    else
    if (lastname.value.length > 30) {
        genError(lastname, "Некорректно указана фамилия!")
    }
    else
    if (!checkCyrillic(lastname.value)) {
        genError(lastname, "Только русские символы!")

    }
    else{
        lastname.style.border = "1px solid black";
    }
}

function checkEmail(){
    var index = mail.value.indexOf("@")

    if ((mail.value.indexOf("@") < 3) ||
        (index > mail.value.length -3) ||
        (mail.value.indexOf(".", index + 2) === -1)
        || mail.value.length > 30) {
        genError(mail, "Ошибка в e-mail!")
        console.log("4*")
    }
    else{
        mail.style.border = "1px solid black";
    }
}
function clheckAgreement(){
    if (checP.checked != true){
        var error = generateErrorMessage("Примите соглашение!");
        checP.parentElement.insertBefore(error, checP);
    }
}

function checkCaptha(){
    if (captha.value != Captha) {
        genError(captha, "Капча не совпадает!");
        randomString(5)
    }
    else{
        captha.style.border = "1px solid black";
    }
}

var Captha;
function randomString(i) {
    var capthText = '';
    var possible = 'abcdefghijklmnopqrstuvwxyz';
    for( var i=0; i < 5; i++ )
        capthText += possible.charAt(Math.floor(Math.random() * possible.length));
    Captha = capthText;
    console.log(Captha);
    document.getElementById("capthaCode").textContent = Captha;

}



function validate_form(){
    //event.preventDefault()
    firstname=document.getElementById('firstname');
    lastname=document.getElementById('lastname');
    mail=document.getElementById('mail');
    password1=document.getElementById('password1');
    password2=document.getElementById('password2');
    captha=document.getElementById('captha');

    fields = document.querySelectorAll('.field')
    removeValidation()
    if (isFieldsFull())
    {
        checkName()
        checkLastname()
        checkEmail()
        checkPassword()
        matchPasswords()
        // clheckAgreement()
        checkCaptha()
    }
    return  isFilled;
}



function check(pass) {
    isFilled = true;
    if(document.getElementById(pass).value.length!==0){
        //alert("true");
        return isFilled;
    }else{
        //alert("false");
        return false;
    }
}