package DAO;

import models.Users;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateSessionFactoryUtil;

import javax.persistence.NoResultException;

public class UsersDAO {
    public Users findUserForLogin(String email, String password) {
        Users user = null;
        try {
            Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
            user = (Users) session.createQuery("FROM Users WHERE email = :mail AND password = :password")
                    .setParameter("mail", email)
                    .setParameter("password", password)
                    .getSingleResult();
        } catch (NoResultException ignore) {
        }
        return user;
    }

    public void saveUser(Users user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(user);
        transaction.commit();
        session.close();
    }

    public void editUser(Users user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(user);
        tx1.commit();
        session.close();
    }

    public void deleteUser(Users user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(user);
        tx1.commit();
        session.close();
    }

    public Users getUser(String email) {
        Users user = null;
        try {
            Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
            user = (Users) session.createQuery("FROM Users WHERE email = :email")
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (NoResultException ignore) {
        }
        return user;
    }

    public int getId(String email) {
        Users user = null;
        try {
            Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
            user = (Users) session.createQuery("FROM Users WHERE email = :email")
                    .setParameter("email", email)
                    .getSingleResult();
            return user.getId();
        } catch (NoResultException ignore) {
        }
        return 0;
    }
}
