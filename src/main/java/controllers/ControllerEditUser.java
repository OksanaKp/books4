package controllers;

import DAO.UsersDAO;
import models.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet("/editing")
public class ControllerEditUser extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UsersDAO userDAO = new UsersDAO();
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html");
        String firstName = req.getParameter("firstname");
        String lastName = req.getParameter("lastname");
        String email = req.getParameter("mail");
        String password = req.getParameter("password1");
        HttpSession session = req.getSession();
        Users updatedUser = (Users) session.getAttribute("userBean");
        if (updatedUser == null) {
            resp.sendRedirect("/books/pageNotFound");
        }
        else {
            Users users = userDAO.getUser(updatedUser.getEmail());
            users.setFirstName(firstName);
            users.setLastName(lastName);
            users.setEmail(email);
            users.setPassword(password);
            userDAO.editUser(users);
            String s = firstName + "<br>" + lastName;
            session.setAttribute("Name", firstName + "<br>" + lastName);
            session.setAttribute("logLink", "Выйти");
            session.setAttribute("userBean", userDAO.getUser(email));
            session.setAttribute("hidIn", "style=\"display: none\"");
            resp.sendRedirect("/books/user/myaccount.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

}
