package controllers;

import DAO.UsersDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/checkEnteredMail")
public class checkMail extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        String answer;
        UsersDAO userDAO = new UsersDAO();
        if (userDAO.getId(req.getParameter("checkedMail")) != 0)
        {
            answer = "<span class='mesage_error'>Логин занят<br></span>";
        }
        else {
            answer = "<span class='success_message'>Логин свободен<br></span>";
        }
        resp.getWriter().write(answer);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
