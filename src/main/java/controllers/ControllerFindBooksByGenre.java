package controllers;
import DAO.BooksDAO;
import models.Books;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/catalog")
public class ControllerFindBooksByGenre  extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BooksDAO booksDAO = new BooksDAO();
        req.setCharacterEncoding("UTF-8");
        String genre = req.getParameter("genre");
        List<Books> books;
        if (genre == null || genre.isEmpty()) {
            books = booksDAO.findAll();
        }
        else {
            books = booksDAO.findAllByGenre(genre);
        }
        req.setAttribute("BookList", books);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/catalog.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
