package controllers;

import DAO.UsersDAO;
import models.Users;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet("/logincheck")
public class ControllerUserLogin extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UsersDAO userDAO = new UsersDAO();
        req.setCharacterEncoding("UTF-8");
        String email = req.getParameter("email");
        String password = req.getParameter("passw");
        HttpSession session = req.getSession();
        Users user = userDAO.findUserForLogin(email, password);
        if (user != null){
            session.setAttribute("userBean", user);
            session.setAttribute("doLogout", "Выйти");
            resp.sendRedirect("user/myaccount.jsp");
        }
        else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pageNotFound");
            requestDispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
